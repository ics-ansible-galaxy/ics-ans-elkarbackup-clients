ics-ans-elkarbackup-clients
===========================

Ansible playbook to install requirements for ElkarBackup clients.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
